/*
Copyright 2022 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package samira

import (
	"context"
	"reflect"
	"testing"
)

func Test_leaseStore_Grant(t *testing.T) {
	type fields struct {
		printer Printer
		client  *client
	}
	type args struct {
		ctx context.Context
		ttl int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name: "grant",
			fields: fields{
				printer: &simple{
					isHex:     false,
					valueOnly: true,
				},
				client: NewMockClient(context.Background()),
			},
			args: args{
				ctx: context.Background(),
				ttl: 10,
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &leaseStore{
				client: tt.fields.client,
			}
			got, err := b.Grant(tt.args.ctx, tt.args.ttl)
			if (err != nil) != tt.wantErr {
				t.Errorf("Grant() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Grant() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_leaseStore_KeepAliveOnce(t *testing.T) {
	type fields struct {
		printer Printer
		client  *client
	}
	type args struct {
		ctx     context.Context
		leaseid int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name: "alive",
			fields: fields{
				printer: &simple{
					isHex:     false,
					valueOnly: true,
				},
				client: NewMockClient(context.Background()),
			},
			args: args{
				ctx:     context.Background(),
				leaseid: 7587862030173429252,
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &leaseStore{
				client: tt.fields.client,
			}
			got, err := b.KeepAliveOnce(tt.args.ctx, tt.args.leaseid)
			if (err != nil) != tt.wantErr {
				t.Errorf("KeepAliveOnce() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("KeepAliveOnce() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_leaseStore_List(t *testing.T) {
	type fields struct {
		printer Printer
		client  *client
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name: "list",
			fields: fields{
				printer: &simple{
					isHex:     false,
					valueOnly: true,
				},
				client: NewMockClient(context.Background()),
			},
			args: args{
				ctx: context.Background(),
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &leaseStore{
				client: tt.fields.client,
			}
			got, err := b.List(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("List() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("List() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_leaseStore_Revoke(t *testing.T) {
	type fields struct {
		printer Printer
		client  *client
	}
	type args struct {
		ctx context.Context
		ttl int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name: "revoke",
			fields: fields{
				printer: &simple{
					isHex:     false,
					valueOnly: true,
				},
				client: NewMockClient(context.Background()),
			},
			args: args{
				ctx: context.Background(),
				ttl: 10,
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &leaseStore{
				client: tt.fields.client,
			}
			got, err := b.Revoke(tt.args.ctx, tt.args.ttl)
			if (err != nil) != tt.wantErr {
				t.Errorf("Revoke() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Revoke() got = %v, want %v", got, tt.want)
			}
		})
	}
}
