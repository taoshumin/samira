/*
Copyright 2022 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package samira

import (
	"context"
	"encoding/json"
	"go.etcd.io/etcd/etcdserver/api/v3rpc/rpctypes"
)

type userStore struct {
	*client
}

// UserAdd Adds a new user
func (a *userStore) UserAdd(ctx context.Context, user, password string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, a.client.requestTimeout)
	defer cancel()

	_, err := a.client.db.UserAdd(ctx, user, password)
	if err != nil {
		return nil, err
	}
	return json.Marshal(a.printer.UserAdd(user))
}

// DeleteUser Deletes a user
func (a *userStore) DeleteUser(ctx context.Context, user string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, a.client.requestTimeout)
	defer cancel()

	_, err := a.client.db.UserDelete(ctx, user)
	if err != nil {
		return nil, err
	}
	return json.Marshal(a.printer.UserDelete(user))
}

// Gets detailed information of a user
func (a *userStore) GetUser(ctx context.Context, name string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, a.client.requestTimeout)
	defer cancel()

	resp, err := a.client.db.UserGet(ctx, name)
	if err != nil {
		return nil, err
	}
	return json.Marshal(a.printer.UserGet(name, *resp))
}

// Users user list.
func (a *userStore) Users(ctx context.Context) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, a.client.requestTimeout)
	defer cancel()

	resp, err := a.client.db.UserList(ctx)
	if err != nil {
		return nil, err
	}
	return json.Marshal(a.printer.UserList(*resp))
}

// ChangePassword Changes password of user
func (a *userStore) ChangePassword(ctx context.Context, username, password string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, a.client.requestTimeout)
	defer cancel()

	_, err := a.client.db.UserChangePassword(ctx, username, password)
	if err != nil {
		return nil, err
	}
	return json.Marshal(a.printer.UserChangePassword())
}

// Grant Grants a role to a user
func (a *userStore) Grant(ctx context.Context, user, role string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, a.client.requestTimeout)
	defer cancel()

	_, err := a.client.db.UserGrantRole(context.Background(), user, role)
	if err != nil {
		return nil, err
	}
	return json.Marshal(a.printer.UserGrantRole(user, role))
}

// Revokes a role from a user
func (a *userStore) Revokes(ctx context.Context, username string, rolename string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, a.client.requestTimeout)
	defer cancel()

	_, err := a.client.db.UserRevokeRole(ctx, username, rolename)
	if err != nil {
		return nil, err
	}
	return json.Marshal(a.printer.UserRevokeRole(username, rolename))
}

// AuthEnable Enables authentication
func (a *userStore) AuthEnable(ctx context.Context, enable bool) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, a.client.requestTimeout)
	defer cancel()

	switch enable {
	case true:
		var err error
		if _, err = a.client.db.AuthEnable(ctx); err == nil {
			return nil, nil
		}

		if err == rpctypes.ErrRootRoleNotExist {
			if _, err = a.client.db.RoleAdd(ctx, "root"); err != nil {
				goto LABEL
			}
			if _, err = a.client.db.UserGrantRole(ctx, "root", "root"); err != nil {
				goto LABEL
			}
		}
	LABEL:
		if err != nil {
			return nil, err
		}
		return json.Marshal(a.printer.Auth(enable))
	default:
		_, err := a.client.db.AuthDisable(ctx)
		if err != nil {
			return nil, err
		}
		return json.Marshal(a.printer.Auth(enable))
	}
}
