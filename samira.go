/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package samira

import (
	"context"
	"encoding/json"
	"io"
	"net/http"

	"go.etcd.io/etcd/clientv3"
)

type KeyValue struct {
	Data []Data `json:"datas,omitempty"`
	// Resp contains error and request status.
	// status is 200 http ok
	// message OK, other error message.
	Response `json:",inline"`
}

type Data struct {
	// ID is lease id.
	TTLID int64 `json:"ttlid,omitempty"`
	// lease ttl default time to expire.
	TTL int64 `json:"ttl,omitempty"`
	// GrantedTTL is the initial granted time in seconds upon lease creation/renewal.
	GrantedTTL int64 `json:"granted_ttl,omitempty"`
	// key is the key in bytes. An empty key is not allowed.
	Key string `json:"key,omitempty"`
	// create_revision is the revision of last creation on this key.
	CreateRevision int64 `json:"create_revision,omitempty"`
	// mod_revision is the revision of last modification on this key.
	ModRevision int64 `json:"mod_revision,omitempty"`
	// version is the version of the key. A deletion resets
	// the version to zero and any modification of the key
	// increases its version.
	Version int64 `json:"version,omitempty"`
	// value is the value held by the key, in bytes.
	Value string `json:"value,omitempty"`
	// lease is the ID of the lease that attached to key.
	// When the attached lease expires, the key will be deleted.
	// If lease is 0, then no lease is attached to the key.
	Lease int64 `json:"lease,omitempty"`
	// size is current key value size
	Size string `json:"size,omitempty"`
	// Members is etcd member information.
	Members `json:",inline"`
	// Status endpoint etcd status.
	Status `json:",inline"`
	// Role user auth role name.
	Role string `json:"role,omitempty"`
	// User use auth user name list.
	User string `json:"user,omitempty"`
}

type Members struct {
	ID         string `json:"id,omitempty"`
	Name       string `json:"name,omitempty"`
	Status     bool   `json:"status,omitempty"`
	PeerAddr   string `json:"peer_addr,omitempty"`
	ClientAddr string `json:"client_addr,omitempty"`
	IsLearner  bool   `json:"is_learner,omitempty"`
}

type Status struct {
	ID               string `json:"id,omitempty"`
	EndPoint         string `json:"end_point,omitempty"`
	EtcdVersion      string `json:"etcd_version,omitempty"`
	DBSize           string `json:"db_size,omitempty"`
	DBSizeInUse      string `json:"db_size_in_use,omitempty"`
	IsLeader         bool   `json:"is_leader,omitempty"`
	IsLearner        bool   `json:"is_learner,omitempty"`
	RaftTerm         string `json:"raft_term,omitempty"`
	RaftIndex        string `json:"raft_index,omitempty"`
	RaftAppliedIndex string `json:"raft_applied_index,omitempty"`
	Errors           string `json:"errors,omitempty"`
}

type Response struct {
	// status is response status
	// 200 is success,otherwise fail
	Status int `json:"status,omitempty"`
	// message is response data message.
	// ok or error message
	Message string `json:"message,omitempty"`
	// operate
	Operate string `json:"operate,omitempty"`
}

type OperateType uint

const (
	GET OperateType = iota + 1
	PUT
	DELETE
	LEASE
	MEMBER
	USER
	ROLE
	ENDPOINT
	UNKNOWN
)

func (o OperateType) String() string {
	switch o {
	case GET:
		return "GET"
	case PUT:
		return "PUT"
	case DELETE:
		return "DELETE"
	case LEASE:
		return "LEASE"
	case MEMBER:
		return "MEMBER"
	case USER:
		return "USER"
	case ROLE:
		return "ROLE"
	case ENDPOINT:
		return "ENDPOINT"
	case UNKNOWN:
		return "UNKNOWN"
	default:
		return "UNKNOWN"
	}
}

func (k *KeyValue) Success(operate OperateType) {
	k.Status = http.StatusOK
	k.Operate = operate.String()
	k.Message = "OK"
}

func (k *KeyValue) SuccessWithMessage(operate OperateType, message string) {
	k.Status = http.StatusOK
	k.Operate = operate.String()
	k.Message = message
}

func (k *KeyValue) Failed(err error, operate OperateType) {
	k.Status = http.StatusBadRequest
	k.Operate = operate.String()
	k.Message = err.Error()
}

func (k *KeyValue) Marshal() []byte {
	b, _ := json.Marshal(k)
	return b
}

type KVClient interface {
	Get() GetStore
	Put() PutStore
	Delete() DeleteStore
	Lease() LeaseStore

	Role() RoleStore
	Member() MemberStore
	User() UserStore

	Endpoints() EndpointStore
	Close() error
}

// Printer - print related operations
type Printer interface {
	Get(resp clientv3.GetResponse) *KeyValue
	Put(resp clientv3.PutResponse) *KeyValue
	Delete(resp clientv3.DeleteResponse) *KeyValue

	LeaseGrant(resp clientv3.LeaseGrantResponse) *KeyValue
	LeaseRevoke(resp clientv3.LeaseRevokeResponse) *KeyValue
	LeaseAlive(resp clientv3.LeaseKeepAliveResponse) *KeyValue
	TimeToLive(resp clientv3.LeaseTimeToLiveResponse, keys bool) *KeyValue
	Leases(resp clientv3.LeaseLeasesResponse) *KeyValue

	MemberAdd(r clientv3.MemberAddResponse) *KeyValue
	MemberRemove(id uint64, r clientv3.MemberRemoveResponse) *KeyValue
	MemberUpdate(id uint64, r clientv3.MemberUpdateResponse) *KeyValue
	MemberList(resp clientv3.MemberListResponse) *KeyValue
	MemberPromote(id uint64, r clientv3.MemberPromoteResponse) *KeyValue

	UserAdd(name string) *KeyValue
	UserDelete(user string) *KeyValue
	UserChangePassword() *KeyValue
	UserGrantRole(user string, role string) *KeyValue
	UserRevokeRole(username string, rolename string) *KeyValue
	UserGet(name string, r clientv3.AuthUserGetResponse) *KeyValue
	UserList(resp clientv3.AuthUserListResponse) *KeyValue
	Auth(open bool) *KeyValue

	Roles(resp clientv3.AuthRoleListResponse) *KeyValue
	RoleAdd(role string) *KeyValue
	RoleDelete(role string) *KeyValue
	RoleGrantPermission(role string) *KeyValue
	GetRole(resp clientv3.AuthRoleGetResponse) *KeyValue

	EndpointStatus(eps []endpointStatus) *KeyValue
}

// PutStore - put related operations
type PutStore interface {
	// Put - related operations
	Put(ctx context.Context, key, value string) ([]byte, error)
	PutWithTTL(ctx context.Context, key, value string, ttl int) ([]byte, error)
	PutKeyWithLease(ctx context.Context, key, value string, leaseid int) ([]byte, error)
	PutKeyWithAliveOnce(ctx context.Context, key, value string, leaseid int) ([]byte, error)
	Puts(ctx context.Context, key, value string, leasid int, putPrevKV, putIgnoreVal, putIgnoreLease bool) ([]byte, error)
}

// GetStore - get related operations
type GetStore interface {
	All(ctx context.Context) ([]byte, error)
	Get(ctx context.Context, key, getConsistency string) ([]byte, error)
	GetX(ctx context.Context, key, endKey, getConsistency, getSortOrder, getSortTarget string, getLimit, getRev int64, getPrefix, getFromKey, getKeysOnly, getCountOnly bool) ([]byte, error)
	GetPrefix(ctx context.Context, prefix string) ([]byte, error)
	SortPrefix(ctx context.Context, prefix, getsortOrder, getsortTarget string) ([]byte, error)
	GetRev(ctx context.Context, key string, getRev int64) ([]byte, error)
}

// DeleteStore - delete related operations
type DeleteStore interface {
	Delete(ctx context.Context, key string) ([]byte, error)
	DeleteALL(ctx context.Context) ([]byte, error)
	DeletePrefix(ctx context.Context, prefix string) ([]byte, error)
}

// LeaseStore - lease related operations
type LeaseStore interface {
	Grant(ctx context.Context, ttl int) ([]byte, error)
	Revoke(ctx context.Context, ttl int) ([]byte, error)
	List(ctx context.Context) ([]byte, error)
	TimeToLive(ctx context.Context, timeToLiveKeys bool, leaseid int64) ([]byte, error)
	KeepAliveOnce(ctx context.Context, leaseid int) ([]byte, error)
}

type RoleStore interface {
	Roles(ctx context.Context) ([]byte, error)
	RoleAdd(ctx context.Context, role string) ([]byte, error)
	DeleteRole(ctx context.Context, role string) ([]byte, error)

	Permission(ctx context.Context, role, key, end string) error
}

type MemberStore interface {
	MemberAdd(ctx context.Context, endpoint []string, learner bool) ([]byte, error)
	MemberRemove(ctx context.Context, id int) ([]byte, error)
	MemberUpdate(ctx context.Context, id int, peerUrl []string) ([]byte, error)
	List(ctx context.Context) ([]byte, error)
	Promotes(ctx context.Context, id int) ([]byte, error)
}

type UserStore interface {
	Users(ctx context.Context) ([]byte, error)
	UserAdd(ctx context.Context, user, password string) ([]byte, error)
	DeleteUser(ctx context.Context, user string) ([]byte, error)
	GetUser(ctx context.Context, name string) ([]byte, error)
	ChangePassword(ctx context.Context, username, password string) ([]byte, error)
	Grant(ctx context.Context, user, role string) ([]byte, error)
	Revokes(ctx context.Context, username string, rolename string) ([]byte, error)
	AuthEnable(ctx context.Context, enable bool) ([]byte, error)
}

type EndpointStore interface {
	EndpointStatus(ctx context.Context) ([]byte, error)
	Ping(ctx context.Context) error
}

// Logger represents an abstracted structured logging implementation.
type Logger interface {
	Debug(...interface{})
	Info(...interface{})
	Error(...interface{})
	Fatal(items ...interface{})
	Panic(items ...interface{})
	Warn(items ...interface{})
	WithField(string, interface{}) Logger

	// Writer Logger can be transformed into an io.Writer.
	Writer() *io.PipeWriter
}
