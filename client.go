/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package samira

import (
	"context"
	"crypto/tls"
	"go.etcd.io/etcd/clientv3"
	"time"
)

const (
	// DefaultDialTimeout is the default dial timeout for the etcd client.
	DefaultDialTimeout = 5 * time.Second
	// DefaultRequestTimeout is the default request timeout for the etcd client.
	DefaultRequestTimeout = 5 * time.Second
	// DefaultDialKeepAliveTime is the default dial keep alive time for the etcd client.
	DefaultDialKeepAliveTime = 10 * time.Second
	// DefaultDialKeepAliveTimeout is the default dial keep alive timeout for the etcd client.
	DefaultDialKeepAliveTimeout = 3 * time.Second
	// DefaultAutoSyncInterval is the default auto sync interval for the etcd client.
	DefaultAutoSyncInterval = 5 * time.Second
	// DefaultEndpoint is the default etcd endpoint.
	DefaultEndpoint = "localhost:2379"
)

// client is a etcd client.
type client struct {
	db             *clientv3.Client
	printer        Printer
	config         clientv3.Config
	requestTimeout time.Duration
	logger         Logger
}

// Option to change behavior of Open()
type Option func(c *client) error

func WithEndpoints(h []string) Option {
	return func(c *client) error {
		if len(h) == 0 {
			return nil
		}
		c.config.Endpoints = h
		return nil
	}
}

func WithDialTimeout(d time.Duration) Option {
	return func(c *client) error {
		if d < 0 {
			return nil
		}
		c.config.DialTimeout = d
		return nil
	}
}

func WithDialKeepAliveTime(d time.Duration) Option {
	return func(c *client) error {
		if d < 0 {
			return nil
		}
		c.config.DialKeepAliveTime = d
		return nil
	}
}

func WithDialKeepAliveTimeout(d time.Duration) Option {
	return func(c *client) error {
		if d < 0 {
			return nil
		}
		c.config.DialKeepAliveTimeout = d
		return nil
	}
}

func WithAutoSyncInterval(d time.Duration) Option {
	return func(c *client) error {
		if d < 0 {
			return nil
		}
		c.config.AutoSyncInterval = d
		return nil
	}
}

func WithRequestTimeout(d time.Duration) Option {
	return func(c *client) error {
		if d < 0 {
			return nil
		}
		c.requestTimeout = d
		return nil
	}
}

func WithLogin(u, p string) Option {
	return func(c *client) error {
		if len(c.config.Username) != 0 && len(c.config.Password) != 0 {
			c.config.Username = u
			c.config.Password = p
		}
		return nil
	}
}

func WithTLS(tlsConfig *tls.Config) Option {
	return func(c *client) error {
		if tlsConfig != nil {
			c.config.TLS = tlsConfig
		}
		return nil
	}
}

func NewClient(ctx context.Context, opts ...Option) (*client, error) {
	c := &client{
		config: clientv3.Config{
			Endpoints:            []string{DefaultEndpoint},
			DialTimeout:          DefaultDialTimeout,
			DialKeepAliveTime:    DefaultDialKeepAliveTime,
			DialKeepAliveTimeout: DefaultDialKeepAliveTimeout,
			AutoSyncInterval:     DefaultAutoSyncInterval,
		},
		requestTimeout: DefaultRequestTimeout,
		printer: &simple{
			isHex:     false,
			valueOnly: true,
		},
	}

	for i := range opts {
		if err := opts[i](c); err != nil {
			return nil, err
		}
	}
	e, err := clientv3.New(c.config)
	if err != nil {
		return nil, err
	}

	c.db = e
	return c, nil
}

// NewMockClient is use for test use only.
func NewMockClient(ctx context.Context) *client {
	c, err := NewClient(ctx, WithEndpoints([]string{"0.0.0.0:2379"}))
	if err != nil {
		panic(err)
	}
	return c
}

func (c *client) Get() GetStore {
	return &getStore{client: c}
}

func (c *client) Put() PutStore {
	return &putStore{client: c}
}

func (c *client) Lease() LeaseStore {
	return &leaseStore{client: c}
}

func (c *client) Delete() DeleteStore {
	return &deleteStore{client: c}
}

func (c *client) Role() RoleStore {
	return &roleStore{client: c}
}

func (c *client) Member() MemberStore {
	return &memberStore{client: c}
}

func (c *client) User() UserStore {
	return &userStore{client: c}
}

func (c *client) Endpoints() EndpointStore {
	return &endpointStore{client: c}
}

func (c *client) Close() error {
	return c.db.Close()
}
