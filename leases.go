/*
Copyright 2022 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package samira

import (
	"context"
	"encoding/json"
	"github.com/spf13/cast"
	"go.etcd.io/etcd/clientv3"
)

type leaseStore struct {
	*client
}

// Grant Creates leases
func (b *leaseStore) Grant(ctx context.Context, ttl int) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, b.client.requestTimeout)
	defer cancel()

	resp, err := b.client.db.Grant(ctx, cast.ToInt64(ttl))
	if err != nil {
		return nil, err
	}
	return json.Marshal(b.printer.LeaseGrant(*resp))
}

// Revoke Revokes leases
func (b *leaseStore) Revoke(ctx context.Context, ttl int) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, b.client.requestTimeout)
	defer cancel()

	resp, err := b.client.db.Revoke(ctx, clientv3.LeaseID(ttl))
	if err != nil {
		return nil, err
	}
	return json.Marshal(b.printer.LeaseRevoke(*resp))
}

// TimeToLive Get lease information
func (b *leaseStore) TimeToLive(ctx context.Context, timeToLiveKeys bool, leaseid int64) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, b.client.requestTimeout)
	defer cancel()

	var opts []clientv3.LeaseOption
	if timeToLiveKeys {
		// Get keys attached to this lease
		opts = append(opts, clientv3.WithAttachedKeys())
	}

	resp, err := b.client.db.TimeToLive(ctx, clientv3.LeaseID(leaseid), opts...)
	if err != nil {
		return nil, err
	}
	return json.Marshal(b.printer.TimeToLive(*resp, timeToLiveKeys))
}

// List all active leases
func (b *leaseStore) List(ctx context.Context) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, b.client.requestTimeout)
	defer cancel()

	resp, err := b.client.db.Leases(ctx)
	if err != nil {
		return nil, err
	}
	return json.Marshal(b.printer.Leases(*resp))
}

// Keeps leases alive (renew)
func (b *leaseStore) KeepAliveOnce(ctx context.Context, leaseid int) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, b.client.requestTimeout)
	defer cancel()

	resp, err := b.client.db.KeepAliveOnce(ctx, clientv3.LeaseID(leaseid))
	if err != nil {
		return nil, err
	}
	return json.Marshal(b.printer.LeaseAlive(*resp))
}
