/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package samira

import (
	"context"
	"encoding/json"
	"github.com/spf13/cast"
	"go.etcd.io/etcd/clientv3"
)

type memberStore struct {
	*client
}

// MemberAdd Adds a member into the cluster
//  endpoints: comma separated peer URLs for the new member
//  learner: indicates if the new member is raft learner
func (c *memberStore) MemberAdd(ctx context.Context, endpoint []string, learner bool) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, c.client.requestTimeout)
	defer cancel()

	var (
		resp *clientv3.MemberAddResponse
		err  error
	)
	if learner {
		resp, err = c.client.db.MemberAddAsLearner(ctx, endpoint)
	} else {
		resp, err = c.client.db.MemberAdd(ctx, endpoint)
	}
	if err != nil {
		return nil, err
	}
	return json.Marshal(c.printer.MemberAdd(*resp))
}

// MemberRemove  Removes a member from the cluster
func (c *memberStore) MemberRemove(ctx context.Context, id int) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, c.client.requestTimeout)
	defer cancel()

	resp, err := c.client.db.MemberRemove(ctx, cast.ToUint64(id))
	if err != nil {
		return nil, err
	}
	return json.Marshal(c.printer.MemberRemove(uint64(id), *resp))
}

// MemberUpdate Updates a member in the cluster
func (c *memberStore) MemberUpdate(ctx context.Context, id int, peerUrl []string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, c.client.requestTimeout)
	defer cancel()

	resp, err := c.client.db.MemberUpdate(ctx, cast.ToUint64(id), peerUrl)
	if err != nil {
		return nil, err
	}
	return json.Marshal(c.printer.MemberUpdate(uint64(id), *resp))
}

// List  all members in the cluster
func (c *memberStore) List(ctx context.Context) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, c.client.requestTimeout)
	defer cancel()

	resp, err := c.client.db.MemberList(ctx)
	if err != nil {
		return nil, err
	}
	return json.Marshal(c.printer.MemberList(*resp))
}

// Promotes a non-voting member in the cluster
func (c *memberStore) Promotes(ctx context.Context, id int) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, c.client.requestTimeout)
	defer cancel()

	resp, err := c.client.db.MemberPromote(ctx, uint64(id))
	if err != nil {
		return nil, err
	}
	return json.Marshal(c.printer.MemberPromote(uint64(id), *resp))
}
