/*
Copyright 2022 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package samira

import (
	"encoding/hex"
	"fmt"
	"strings"

	"github.com/dustin/go-humanize"
	"go.etcd.io/etcd/clientv3"
	"go.etcd.io/etcd/mvcc/mvccpb"
)

type simple struct {
	isHex     bool
	valueOnly bool
}

func NewErrors(err error, operate OperateType) *KeyValue {
	kvo := &KeyValue{}
	kvo.Failed(err, operate)
	return kvo
}

func (s *simple) Get(resp clientv3.GetResponse) *KeyValue {
	kvs := make([]Data, len(resp.Kvs))
	kvo := &KeyValue{}
	for i, kv := range resp.Kvs {
		k, v := printKV(s.isHex, s.valueOnly, kv)
		data := Data{
			Key:            k,
			CreateRevision: kv.CreateRevision,
			ModRevision:    kv.ModRevision,
			Version:        kv.Version,
			Value:          v,
			Lease:          kv.Lease,
			Size:           humanize.Bytes(uint64(len(v))),
		}
		// append kvs
		kvs[i] = data
	}
	kvo.Data = append(kvo.Data, kvs...)
	kvo.Success(GET)
	return kvo
}

func (s *simple) Put(resp clientv3.PutResponse) *KeyValue {
	kvo := &KeyValue{}
	if resp.PrevKv != nil {
		k, v := printKV(s.isHex, s.valueOnly, resp.PrevKv)
		data := Data{
			Key:            k,
			CreateRevision: resp.PrevKv.CreateRevision,
			ModRevision:    resp.PrevKv.ModRevision,
			Version:        resp.PrevKv.Version,
			Value:          v,
			Lease:          resp.PrevKv.Lease,
			Size:           humanize.Bytes(uint64(len(v))),
		}
		kvo.Data = append(kvo.Data, data)
	}
	kvo.Success(PUT)
	return kvo
}

func (s *simple) Delete(resp clientv3.DeleteResponse) *KeyValue {
	kvs := make([]Data, len(resp.PrevKvs))
	kvo := &KeyValue{}
	for i, kv := range resp.PrevKvs {
		// get key value
		k, v := printKV(s.isHex, s.valueOnly, kv)
		// key value object
		data := Data{
			Key:            k,
			CreateRevision: kv.CreateRevision,
			ModRevision:    kv.ModRevision,
			Version:        kv.Version,
			Value:          v,
			Lease:          kv.Lease,
			Size:           humanize.Bytes(uint64(len(v))),
		}
		// append kvs
		kvs[i] = data
	}
	kvo.Data = append(kvo.Data, kvs...)
	kvo.Success(DELETE)
	return kvo
}

func (s *simple) LeaseAlive(resp clientv3.LeaseKeepAliveResponse) *KeyValue {
	kvo := &KeyValue{}
	data := Data{
		TTLID: int64(resp.ID),
		TTL:   resp.TTL,
	}
	kvo.Data = append(kvo.Data, data)
	kvo.Success(LEASE)
	return kvo
}

func (s *simple) LeaseGrant(resp clientv3.LeaseGrantResponse) *KeyValue {
	kvo := &KeyValue{}
	data := Data{
		// maybe change %016x
		TTLID: int64(resp.ID),
		TTL:   resp.TTL,
	}
	kvo.Data = append(kvo.Data, data)
	kvo.Success(LEASE)
	return kvo
}

func (s *simple) LeaseRevoke(resp clientv3.LeaseRevokeResponse) *KeyValue {
	kvo := &KeyValue{}
	kvo.Success(LEASE)
	return kvo
}

func (s *simple) TimeToLive(resp clientv3.LeaseTimeToLiveResponse, keys bool) *KeyValue {
	kvo := &KeyValue{}
	if resp.GrantedTTL == 0 && resp.TTL == -1 {
		kvo.SuccessWithMessage(LEASE, fmt.Sprintf("lease %016x already expired\n", resp.ID))
		return kvo
	}

	txt := fmt.Sprintf("lease %016x granted with TTL(%ds), remaining(%ds)", resp.ID, resp.GrantedTTL, resp.TTL)
	if keys {
		ks := make([]string, len(resp.Keys))
		for i := range resp.Keys {
			ks[i] = string(resp.Keys[i])
		}
		txt += fmt.Sprintf(", attached keys(%v)", ks)
	}
	kvo.SuccessWithMessage(LEASE, txt)
	return kvo
}

func (s *simple) Leases(resp clientv3.LeaseLeasesResponse) *KeyValue {
	kvo := &KeyValue{}
	for _, lease := range resp.Leases {
		kvo.Data = append(kvo.Data, Data{TTLID: int64(lease.ID)})
	}
	kvo.Success(LEASE)
	return kvo
}

func (s *simple) MemberAdd(r clientv3.MemberAddResponse) *KeyValue {
	msg := fmt.Sprintf("Member %16x added to cluster %16x\n", r.Member.ID, r.Header.ClusterId)
	var conf []string
	for _, member := range r.Members {
		for _, u := range member.PeerURLs {
			n := member.Name
			conf = append(conf, fmt.Sprintf("%s=%s", n, u))
		}
	}

	msg += fmt.Sprintf("\n")
	msg += fmt.Sprintf("ETCD_INITIAL_CLUSTER=%q\n", strings.Join(conf, ","))
	kvo := &KeyValue{}
	kvo.SuccessWithMessage(MEMBER, msg)
	return kvo
}

func (s *simple) MemberRemove(id uint64, r clientv3.MemberRemoveResponse) *KeyValue {
	msg := fmt.Sprintf("Member %16x removed from cluster %16x\n", id, r.Header.ClusterId)
	kvo := &KeyValue{}
	kvo.SuccessWithMessage(MEMBER, msg)
	return kvo
}

func (s *simple) MemberUpdate(id uint64, r clientv3.MemberUpdateResponse) *KeyValue {
	msg := fmt.Sprintf("Member %16x updated in cluster %16x\n", id, r.Header.ClusterId)
	kvo := &KeyValue{}
	kvo.SuccessWithMessage(MEMBER, msg)
	return kvo
}

func (s *simple) MemberList(resp clientv3.MemberListResponse) *KeyValue {
	kvo := &KeyValue{}
	for _, m := range resp.Members {
		ok := true
		if len(m.Name) == 0 {
			ok = false
		}
		meb := Members{
			ID:         fmt.Sprintf("%x", m.ID),
			Name:       m.Name,
			Status:     ok,
			PeerAddr:   strings.Join(m.PeerURLs, ","),
			ClientAddr: strings.Join(m.ClientURLs, ","),
			IsLearner:  m.IsLearner,
		}
		kvo.Data = append(kvo.Data, Data{
			Members: meb,
		})
	}
	kvo.Success(MEMBER)
	return kvo
}

func (s *simple) MemberPromote(id uint64, r clientv3.MemberPromoteResponse) *KeyValue {
	msg := fmt.Sprintf("Member %16x promoted in cluster %16x\n", id, r.Header.ClusterId)
	kvo := &KeyValue{}
	kvo.SuccessWithMessage(MEMBER, msg)
	return kvo
}

func (s *simple) EndpointStatus(eps []endpointStatus) *KeyValue {
	kvo := &KeyValue{}
	for _, status := range eps {
		stu := Status{
			ID:               fmt.Sprintf("%x", status.Resp.Header.MemberId),
			EndPoint:         status.Endpoint,
			EtcdVersion:      status.Resp.Version,
			DBSize:           humanize.Bytes(uint64(status.Resp.DbSize)),
			DBSizeInUse:      humanize.Bytes(uint64(status.Resp.DbSizeInUse)),
			IsLeader:         status.Resp.Leader == status.Resp.Header.MemberId,
			IsLearner:        status.Resp.IsLearner,
			RaftTerm:         fmt.Sprint(status.Resp.RaftTerm),
			RaftIndex:        fmt.Sprint(status.Resp.RaftIndex),
			RaftAppliedIndex: fmt.Sprint(status.Resp.RaftAppliedIndex),
			Errors:           fmt.Sprint(strings.Join(status.Resp.Errors, ", ")),
		}
		kvo.Data = append(kvo.Data, Data{
			Status: stu,
		})
	}
	kvo.Success(ENDPOINT)
	return kvo
}

func (s *simple) UserAdd(name string) *KeyValue {
	msg := fmt.Sprintf("User %s created\n", name)
	kvo := &KeyValue{}
	kvo.SuccessWithMessage(USER, msg)
	return kvo
}

func (s *simple) UserDelete(user string) *KeyValue {
	msg := fmt.Sprintf("User %s deleted\n", user)
	kvo := &KeyValue{}
	kvo.SuccessWithMessage(USER, msg)
	return kvo
}

func (s *simple) UserGet(name string, r clientv3.AuthUserGetResponse) *KeyValue {
	kvo := &KeyValue{}
	for _, role := range r.Roles {
		kvo.Data = append(kvo.Data, Data{
			Role: role,
		})
	}
	kvo.Success(USER)
	return kvo
}

func (s *simple) UserChangePassword() *KeyValue {
	msg := fmt.Sprint("Password Update")
	kvo := &KeyValue{}
	kvo.SuccessWithMessage(USER, msg)
	return kvo
}

func (s *simple) UserGrantRole(user string, role string) *KeyValue {
	msg := fmt.Sprintf("Role %s is granted to user %s\n", role, user)
	kvo := &KeyValue{}
	kvo.SuccessWithMessage(USER, msg)
	return kvo
}

func (s *simple) UserRevokeRole(username string, rolename string) *KeyValue {
	msg := fmt.Sprintf("Role %s is revoked from user %s\n", rolename, username)
	kvo := &KeyValue{}
	kvo.SuccessWithMessage(USER, msg)
	return kvo
}

func (s *simple) UserList(resp clientv3.AuthUserListResponse) *KeyValue {
	kvo := &KeyValue{}
	for _, usr := range resp.Users {
		kvo.Data = append(kvo.Data, Data{
			User: usr,
		})
	}
	kvo.Success(USER)
	return kvo
}

func (s *simple) Auth(open bool) *KeyValue {
	var msg string
	if open {
		msg = fmt.Sprintf("Authentication Enabled")
	} else {
		msg = fmt.Sprintf("Authentication Disabled")
	}
	kvo := &KeyValue{}
	kvo.SuccessWithMessage(USER, msg)
	return kvo
}

func (s *simple) GetRole(resp clientv3.AuthRoleGetResponse) *KeyValue {
	return nil
}

func (s *simple) Roles(resp clientv3.AuthRoleListResponse) *KeyValue {
	kvo := &KeyValue{}
	for _, role := range resp.Roles {
		kvo.Data = append(kvo.Data, Data{
			Role: role,
		})
	}
	kvo.Success(ROLE)
	return kvo
}

func (s *simple) RoleAdd(role string) *KeyValue {
	msg := fmt.Sprintf("Role %s created\n", role)
	kvo := &KeyValue{}
	kvo.SuccessWithMessage(ROLE, msg)
	return kvo
}

func (s *simple) RoleDelete(role string) *KeyValue {
	msg := fmt.Sprintf("Role %s deleted\n", role)
	kvo := &KeyValue{}
	kvo.SuccessWithMessage(ROLE, msg)
	return kvo
}

func (s *simple) RoleGrantPermission(role string) *KeyValue {
	msg := fmt.Sprintf("Role %s updated\n", role)
	kvo := &KeyValue{}
	kvo.SuccessWithMessage(ROLE, msg)
	return kvo
}

func printKV(isHex bool, valueOnly bool, kv *mvccpb.KeyValue) (string, string) {
	k, v := string(kv.Key), string(kv.Value)
	if isHex {
		k = addHexPrefix(hex.EncodeToString(kv.Key))
		v = addHexPrefix(hex.EncodeToString(kv.Value))
	}
	if !valueOnly {
		return k, ""
	}
	return k, v
}

func addHexPrefix(s string) string {
	ns := make([]byte, len(s)*2)
	for i := 0; i < len(s); i += 2 {
		ns[i*2] = '\\'
		ns[i*2+1] = 'x'
		ns[i*2+2] = s[i]
		ns[i*2+3] = s[i+1]
	}
	return string(ns)
}
