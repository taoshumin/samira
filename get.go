/*
Copyright 2022 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package samira

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"

	"go.etcd.io/etcd/clientv3"
)

//getStore is a key value bucket.
// Get - Related operations
type getStore struct {
	*client
}

func (b *getStore) All(ctx context.Context) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, b.client.requestTimeout)
	defer cancel()

	//key := "\x00"
	// if you only want key ,use key = \x00
	resp, err := b.client.db.Get(ctx, "", clientv3.WithFromKey())
	if err != nil {
		return nil, err
	}
	return json.Marshal(b.printer.Get(*resp))
}

func (b *getStore) GetX(
	ctx context.Context,
	key,
	endKey,
	getConsistency,
	getSortOrder,
	getSortTarget string,
	getLimit,
	getRev int64,
	getPrefix,
	getFromKey,
	getKeysOnly,
	getCountOnly bool) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, b.client.requestTimeout)
	defer cancel()

	var opts []clientv3.OpOption

	// linearizable(l) or Serializable(s)
	switch getConsistency {
	case "s":
		opts = append(opts, clientv3.WithSerializable())
	case "l":
	default:
		return nil, fmt.Errorf("unknown consistency flag %v", getConsistency)
	}

	if len(endKey) > 0 {
		if getPrefix || getFromKey {

			return nil, fmt.Errorf("too many arguments, only accept one argument when `--prefix` or `--from-key` is set")
		}
		opts = append(opts, clientv3.WithRange(endKey))
	}

	// maximum number of results
	opts = append(opts, clientv3.WithLimit(getLimit))
	if getRev > 0 {
		opts = append(opts, clientv3.WithRev(getRev))
	}

	// Order of results; ASCEND or DESCEND (ASCEND by default)
	sortByOrder := clientv3.SortNone
	sortOrder := strings.ToUpper(getSortOrder)
	switch sortOrder {
	case "ASCEND":
		sortByOrder = clientv3.SortAscend
	case "DESCEND":
		sortByOrder = clientv3.SortDescend
	case "":
		// nothing
	default:
		err := fmt.Errorf("bad sort order %v", getSortOrder)
		return nil, err
	}

	// Sort target; CREATE, KEY, MODIFY, VALUE, or VERSION
	sortByTarget := clientv3.SortByKey
	sortTarget := strings.ToUpper(getSortTarget)
	switch sortTarget {
	case "CREATE":
		sortByTarget = clientv3.SortByCreateRevision
	case "KEY":
		sortByTarget = clientv3.SortByKey
	case "MODIFY":
		sortByTarget = clientv3.SortByModRevision
	case "VALUE":
		sortByTarget = clientv3.SortByValue
	case "VERSION":
		sortByTarget = clientv3.SortByVersion
	case "":
		// nothing
	default:
		err := fmt.Errorf("bad sort target %v", getSortTarget)
		return nil, err
	}

	opts = append(opts, clientv3.WithSort(sortByTarget, sortByOrder))

	// Get keys with matching prefix
	if getPrefix {
		if len(key) == 0 {
			key = "\x00"
			opts = append(opts, clientv3.WithFromKey())
		} else {
			opts = append(opts, clientv3.WithPrefix())
		}
	}

	// Get keys that are greater than or equal to the given key using byte compare
	if getFromKey {
		if len(key) == 0 {
			key = "\x00"
		}
		opts = append(opts, clientv3.WithFromKey())
	}

	// Get only the keys
	if getKeysOnly {
		opts = append(opts, clientv3.WithKeysOnly())
	}

	// Get only the count
	if getCountOnly {
		opts = append(opts, clientv3.WithCountOnly())
	}

	resp, err := b.client.db.Get(ctx, key, opts...)
	if err != nil {
		return nil, err
	}
	return json.Marshal(b.printer.Get(*resp))
}

func (b *getStore) Get(ctx context.Context, key, getConsistency string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, b.client.requestTimeout)
	defer cancel()

	var opts []clientv3.OpOption

	// linearizable(l) or Serializable(s)
	switch getConsistency {
	case "s":
		opts = append(opts, clientv3.WithSerializable())
	case "l":
	default:
		return nil, fmt.Errorf("unknown consistency flag %q", getConsistency)
	}

	resp, err := b.client.db.Get(ctx, key, opts...)
	if err != nil {
		return nil, err
	}
	return json.Marshal(b.printer.Get(*resp))
}

func (b *getStore) GetPrefix(ctx context.Context, prefix string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, b.client.requestTimeout)
	defer cancel()

	var opts []clientv3.OpOption
	if len(prefix) == 0 {
		opts = append(opts, clientv3.WithFromKey())
	} else {
		opts = append(opts, clientv3.WithPrefix())
	}
	resp, err := b.client.db.Get(ctx, prefix, opts...)
	if err != nil {
		return nil, err
	}
	return json.Marshal(b.printer.Get(*resp))
}

func (b *getStore) SortPrefix(ctx context.Context, prefix, getsortOrder, getsortTarget string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, b.client.requestTimeout)
	defer cancel()

	var opts []clientv3.OpOption
	if len(prefix) == 0 {
		opts = append(opts, clientv3.WithFromKey())
	} else {
		opts = append(opts, clientv3.WithPrefix())
	}

	sortByOrder := clientv3.SortNone
	sortOrder := strings.ToUpper(getsortOrder)
	switch sortOrder {
	case "ASCEND":
		sortByOrder = clientv3.SortAscend
	case "DESCEND":
		sortByOrder = clientv3.SortDescend
	case "":
		// nothing
	default:
		return nil, fmt.Errorf("bad sort order %v", getsortOrder)
	}

	sortByTarget := clientv3.SortByKey
	sortTarget := strings.ToUpper(getsortTarget)
	switch sortTarget {
	case "CREATE":
		sortByTarget = clientv3.SortByCreateRevision
	case "KEY":
		sortByTarget = clientv3.SortByKey
	case "MODIFY":
		sortByTarget = clientv3.SortByModRevision
	case "VALUE":
		sortByTarget = clientv3.SortByValue
	case "VERSION":
		sortByTarget = clientv3.SortByVersion
	case "":
		// nothing
	default:
		return nil, fmt.Errorf("bad sort target %v", getsortTarget)
	}

	opts = append(opts, clientv3.WithSort(sortByTarget, sortByOrder))
	resp, err := b.client.db.Get(ctx, prefix, opts...)
	if err != nil {
		return nil, err
	}
	return json.Marshal(b.printer.Get(*resp))
}

func (b *getStore) GetRev(ctx context.Context, key string, getRev int64) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, b.client.requestTimeout)
	defer cancel()

	var opts []clientv3.OpOption
	if len(key) == 0 {
		opts = append(opts, clientv3.WithFromKey())
	}

	if getRev > 0 {
		opts = append(opts, clientv3.WithRev(getRev))
	}

	resp, err := b.client.db.Get(ctx, key, opts...)
	if err != nil {
		return nil, err
	}
	return json.Marshal(b.printer.Get(*resp))
}
