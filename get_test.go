/*
Copyright 2022 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package samira

import (
	"context"
	"reflect"
	"testing"
)

func Test_getStore_All(t *testing.T) {
	type fields struct {
		printer Printer
		client  *client
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name: "all",
			fields: fields{
				printer: &simple{
					isHex:     false,
					valueOnly: true,
				},
				client: NewMockClient(context.Background()),
			},
			args: args{
				ctx: context.Background(),
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &getStore{
				client: tt.fields.client,
			}
			got, err := b.All(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("All() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("All() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getStore_Get(t *testing.T) {
	type fields struct {
		printer Printer
		client  *client
	}
	type args struct {
		ctx            context.Context
		key            string
		getConsistency string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name: "add",
			fields: fields{
				printer: &simple{
					isHex:     false,
					valueOnly: true,
				},
				client: NewMockClient(context.Background()),
			},
			args: args{
				ctx:            context.Background(),
				key:            "aaaa/bbbb/cccc",
				getConsistency: "l",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &getStore{
				client: tt.fields.client,
			}
			got, err := b.Get(tt.args.ctx, tt.args.key, tt.args.getConsistency)
			if (err != nil) != tt.wantErr {
				t.Errorf("Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Get() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getStore_GetPrefix(t *testing.T) {
	type fields struct {
		printer Printer
		client  *client
	}
	type args struct {
		ctx    context.Context
		prefix string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name: "prix",
			fields: fields{
				printer: &simple{
					isHex:     false,
					valueOnly: false,
				},
				client: NewMockClient(context.Background()),
			},
			args: args{
				ctx:    context.Background(),
				prefix: "a",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &getStore{
				client: tt.fields.client,
			}
			got, err := b.GetPrefix(tt.args.ctx, tt.args.prefix)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetPrefix() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetPrefix() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getStore_GetRev(t *testing.T) {
	type fields struct {
		printer Printer
		client  *client
	}
	type args struct {
		ctx    context.Context
		key    string
		getRev int64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name: "rev",
			fields: fields{
				printer: &simple{
					isHex:     false,
					valueOnly: false,
				},
				client: NewMockClient(context.Background()),
			},
			args: args{
				ctx:    context.Background(),
				key:    "a",
				getRev: 1,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &getStore{
				client: tt.fields.client,
			}
			got, err := b.GetRev(tt.args.ctx, tt.args.key, tt.args.getRev)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetRev() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetRev() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getStore_GetX(t *testing.T) {
	type fields struct {
		printer Printer
		client  *client
	}
	type args struct {
		ctx            context.Context
		key            string
		endKey         string
		getConsistency string
		getSortOrder   string
		getSortTarget  string
		getLimit       int64
		getRev         int64
		getPrefix      bool
		getFromKey     bool
		getKeysOnly    bool
		getCountOnly   bool
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name: "getx",
			fields: fields{
				printer: &simple{
					isHex:     false,
					valueOnly: false,
				},
				client: NewMockClient(context.Background()),
			},
			args: args{
				ctx:            context.Background(),
				key:            "a",
				endKey:         "",
				getConsistency: "s",
				getSortOrder:   "ASCEND",
				getSortTarget:  "CREATE",
				getLimit:       10,
				getRev:         1,
				getPrefix:      true,
				getFromKey:     true,
				getKeysOnly:    false,
				getCountOnly:   false,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &getStore{
				client: tt.fields.client,
			}
			got, err := b.GetX(tt.args.ctx, tt.args.key, tt.args.endKey, tt.args.getConsistency, tt.args.getSortOrder, tt.args.getSortTarget, tt.args.getLimit, tt.args.getRev, tt.args.getPrefix, tt.args.getFromKey, tt.args.getKeysOnly, tt.args.getCountOnly)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetX() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetX() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getStore_SortPrefix(t *testing.T) {
	type fields struct {
		printer Printer
		client  *client
	}
	type args struct {
		ctx           context.Context
		prefix        string
		getsortOrder  string
		getsortTarget string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name: "sort",
			fields: fields{
				printer: &simple{
					isHex:     false,
					valueOnly: false,
				},
				client: NewMockClient(context.Background()),
			},
			args: args{
				ctx:           context.Background(),
				prefix:        "a",
				getsortOrder:  "ASCEND",
				getsortTarget: "CREATE",
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &getStore{
				client: tt.fields.client,
			}
			got, err := b.SortPrefix(tt.args.ctx, tt.args.prefix, tt.args.getsortOrder, tt.args.getsortTarget)
			if (err != nil) != tt.wantErr {
				t.Errorf("SortPrefix() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SortPrefix() got = %v, want %v", got, tt.want)
			}
		})
	}
}
