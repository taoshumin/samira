/*
Copyright 2022 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package samira

import (
	"context"
	"encoding/json"
	"go.etcd.io/etcd/clientv3"
)

type deleteStore struct {
	*client
}

// Delete delete the key.
func (b *deleteStore) Delete(ctx context.Context, key string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, b.client.requestTimeout)
	defer cancel()

	var opts []clientv3.OpOption
	// returns deleted key-value pairs
	opts = append(opts, clientv3.WithPrevKV())

	resp, err := b.client.db.Delete(ctx, key, opts...)
	if err != nil {
		return nil, err
	}
	return json.Marshal(b.printer.Delete(*resp))
}

// DeleteALL delete all keys
func (b *deleteStore) DeleteALL(ctx context.Context) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, b.client.requestTimeout)
	defer cancel()

	var opts []clientv3.OpOption
	// returns deleted key-value pairs
	opts = append(opts, clientv3.WithPrevKV())
	opts = append(opts, clientv3.WithFromKey())

	//key := "\x00"
	resp, err := b.client.db.Delete(ctx, "", opts...)
	if err != nil {
		return nil, err
	}
	return json.Marshal(b.printer.Delete(*resp))
}

// DeletePrefix delete keys with matching prefix
func (b *deleteStore) DeletePrefix(ctx context.Context, prefix string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, b.client.requestTimeout)
	defer cancel()

	var opts []clientv3.OpOption
	// returns deleted key-value pairs
	opts = append(opts, clientv3.WithPrevKV())
	opts = append(opts, clientv3.WithPrefix())

	resp, err := b.client.db.Delete(ctx, prefix, opts...)
	if err != nil {
		return nil, err
	}
	return json.Marshal(b.printer.Delete(*resp))
}
