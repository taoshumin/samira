/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package samira

import (
	"context"
	"encoding/json"
	"github.com/spf13/cast"
	"go.etcd.io/etcd/clientv3"
)

type putStore struct {
	printer Printer
	client  *client
}

// Put the given key into the store
func (b *putStore) Put(ctx context.Context, key, value string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, b.client.requestTimeout)
	defer cancel()

	resp, err := b.client.db.Put(ctx, key, value)
	if err != nil {
		return nil, err
	}
	return json.Marshal(b.printer.Put(*resp))
}

func (b *putStore) PutWithTTL(ctx context.Context, key, value string, ttl int) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, b.client.requestTimeout)
	defer cancel()

	resp, err := b.client.db.Grant(ctx, cast.ToInt64(ttl))
	if err != nil {
		return nil, err
	}
	respp, err := b.client.db.Put(ctx, key, value, clientv3.WithLease(clientv3.LeaseID(resp.ID)))
	if err != nil {
		return nil, err
	}
	return json.Marshal(b.printer.Put(*respp))
}

func (b *putStore) PutKeyWithLease(ctx context.Context, key, value string, leaseid int) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, b.client.requestTimeout)
	defer cancel()

	resp, err := b.client.db.Put(ctx, key, value, clientv3.WithLease(clientv3.LeaseID(leaseid)))
	if err != nil {
		return nil, err
	}
	return json.Marshal(b.printer.Put(*resp))
}

func (b *putStore) PutKeyWithAliveOnce(ctx context.Context, key, value string, ttl int) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, b.client.requestTimeout)
	defer cancel()
	// get LeaseID
	resp, err := b.client.db.Grant(ctx, cast.ToInt64(ttl))
	if err != nil {
		return nil, err
	}
	// put LeaseID
	_, err = b.client.db.Put(ctx, key, value, clientv3.WithLease(clientv3.LeaseID(resp.ID)))
	if err != nil {
		return nil, err
	}
	// keep alive
	ressp, err := b.client.db.KeepAliveOnce(ctx, clientv3.LeaseID(resp.ID))
	if err != nil {
		return nil, err
	}
	return json.Marshal(b.printer.LeaseAlive(*ressp))
}

// Puts the given key into the store
// lease ID (in hexadecimal) to attach to the key"
// return the previous key-value pair before modification
// updates the key using its current value
// updates the key using its current lease
func (b putStore) Puts(ctx context.Context, key, value string, leasid int, putPrevKV, putIgnoreVal, putIgnoreLease bool) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, b.client.requestTimeout)
	defer cancel()

	var opts []clientv3.OpOption
	if leasid != 0 {
		opts = append(opts, clientv3.WithLease(clientv3.LeaseID(leasid)))
	}

	if putPrevKV {
		opts = append(opts, clientv3.WithPrevKV())
	}
	if putIgnoreVal {
		opts = append(opts, clientv3.WithIgnoreValue())
	}
	if putIgnoreLease {
		opts = append(opts, clientv3.WithIgnoreLease())
	}

	resp, err := b.client.db.Put(ctx, key, value, opts...)
	if err != nil {
		return nil, err
	}
	return json.Marshal(b.printer.Put(*resp))
}
