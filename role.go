/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package samira

import (
	"context"
	"encoding/json"
	"go.etcd.io/etcd/clientv3"
)

type roleStore struct {
	*client
}

func (a *roleStore) Get(ctx context.Context, role string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, a.client.requestTimeout)
	defer cancel()

	resp, err := a.client.db.RoleGet(ctx, role)
	if err != nil {
		return nil, err
	}
	return json.Marshal(a.printer.GetRole(*resp))
}

func (a *roleStore) Roles(ctx context.Context) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, a.client.requestTimeout)
	defer cancel()

	resp, err := a.client.db.RoleList(ctx)
	if err != nil {
		return nil, err
	}
	return json.Marshal(a.printer.Roles(*resp))
}

func (a *roleStore) RoleAdd(ctx context.Context, role string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, a.client.requestTimeout)
	defer cancel()

	_, err := a.client.db.RoleAdd(ctx, role)
	if err != nil {
		return nil, err
	}
	return json.Marshal(a.printer.RoleAdd(role))
}

func (a *roleStore) DeleteRole(ctx context.Context, role string) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, a.client.requestTimeout)
	defer cancel()

	_, err := a.client.db.RoleDelete(ctx, role)
	if err != nil {
		return nil, err
	}
	return json.Marshal(a.printer.RoleDelete(role))
}

func (a *roleStore) Permission(ctx context.Context, role, key, end string) error {
	ctx, cancel := context.WithTimeout(ctx, a.client.requestTimeout)
	defer cancel()

	// for example key rangEnd foo ,zoo.
	_, err := a.client.db.RoleGrantPermission(context.Background(),
		role,
		key,
		end,
		clientv3.PermissionType(clientv3.PermReadWrite))
	return err
}
