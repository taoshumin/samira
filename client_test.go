/*
Copyright 2022 The SHUMIN Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package samira

import (
	"context"
	"reflect"
	"testing"
)

func TestNewClient(t *testing.T) {
	type args struct {
		ctx  context.Context
		opts []Option
	}
	tests := []struct {
		name    string
		args    args
		want    *client
		wantErr bool
	}{
		{
			name: "测试etcd集群",
			args: args{
				ctx: context.Background(),
				opts: []Option{
					WithEndpoints([]string{
						"106.75.53.230:32373",
						"106.75.53.230:32374",
						"106.75.53.230:32375",
						"106.75.53.230:32376",
						"106.75.53.230:32377",
					}),
				},
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewClient(tt.args.ctx, tt.args.opts...)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewClient() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			_, err = got.Put().Put(context.Background(), "aaa", "bbb")
			if err != nil {
				t.Error(err)
			}
			v, err := got.Get().Get(context.Background(), "aaa", "s")
			if err != nil {
				t.Error(err)
			}
			t.Log(v)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewClient() got = %v, want %v", got, tt.want)
			}
		})
	}
}
