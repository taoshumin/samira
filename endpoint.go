/*
Copyright 2022 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package samira

import (
	"context"
	"encoding/json"
	"go.etcd.io/etcd/clientv3"
	"go.etcd.io/etcd/etcdserver/api/v3rpc/rpctypes"
)

type endpointStore struct {
	*client
}

type endpointStatus struct {
	Endpoint string                   `json:"Endpoint"`
	Resp     *clientv3.StatusResponse `json:"Status"`
}

// EndpointStatus Prints out the status of endpoints specified in `--endpoints` flag
func (e *endpointStore) EndpointStatus(ctx context.Context) ([]byte, error) {
	ctx, cancel := context.WithTimeout(ctx, e.client.requestTimeout)
	defer cancel()

	resp, err := e.client.db.MemberList(ctx)
	if err != nil {
		return nil, err
	}

	var members []string
	for _, member := range resp.Members {
		members = append(members, member.ClientURLs...)
	}

	list := []endpointStatus{}
	for _, member := range members {
		resp, err := e.client.db.Status(ctx, member)
		if err != nil {
			continue
		}
		list = append(list, endpointStatus{
			Endpoint: member,
			Resp:     resp,
		})
	}
	return json.Marshal(e.printer.EndpointStatus(list))
}

func (e *endpointStore) Ping(ctx context.Context) error {
	ctx, cancel := context.WithTimeout(ctx, e.client.requestTimeout)
	defer cancel()
	_, err := e.client.db.Get(ctx, "health")
	if err == nil || err == rpctypes.ErrPermissionDenied {
		return nil
	} else {
		return err
	}
}
