/*
Copyright 2022 The SHUMIN Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package samira

import (
	"context"
	"reflect"
	"testing"
)

func Test_deleteStore_DeleteALL(t *testing.T) {
	type fields struct {
		client *client
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name: "delete all",
			fields: fields{
				client: NewMockClient(context.Background()),
			},
			args: args{
				ctx: context.Background(),
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &deleteStore{
				client: tt.fields.client,
			}
			got, err := b.DeleteALL(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("DeleteALL() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DeleteALL() got = %v, want %v", got, tt.want)
			}
		})
	}
}
